Adding/Changing ASN.1 dictionaries should be done by modifying the files
in the asn1 directory.

After any change of ASN.1 dictionaries

    rebar3 asn compile

should be invoked from the directory of the ss7_asn application (this one)

After compiling the ASN.1 files only the header and erl source files are
generated. These could be compiled e.g. with rebar3 compile.

If cleaning of the generated files is required (e.g. to clean the
directories before pushing to the repository), run

    rebar3 asn clean

Considering that these files should not change often, the generated files
could be pushed to the code repository, so normal initial project
compilation would work after checking out the project (for those who
normally don't have to deal with ASN.1 files).

ASN.1 compilation is quite slow this way, however the interdependencies
among the files (picked from specs) is quite messy so a proper Makefile
is rather tedious to get right. Might be doing it later eventually.

In order to the above ASN.1 compilation you need to have the rebar3 asn1
provider configured in ~/.config/rebar3/rebar.config

e.g.

    {plugins, [
        rebar3_auto,
        rebar3_hex,
        provider_asn1
    ]}.

during development, rebar version 3.3.1 was used
